library(dplyr)
library(readr)
library(tidyr)

directory <- 'data/V2/raw/'
csv_files <- list.files(directory, pattern = "*.csv", full.names=TRUE)
combined_df <- NULL

for (id in 8:14){
  print(csv_files[[id]])
  df <- read_delim(csv_files[[id]], delim=';')
  combined_df <- bind_rows(combined_df,df)
}
df=NULL

head(combined_df)
summary(combined_df)

##### First set of data

#prep

combined_df <- combined_df %>%
  rename('date'='TS',
         'station'='ID PDC local',
         'address'='Adresse station',
         'postcode'='Code INSEE commune',
         'status'='Statut du point de recharge') %>%
  mutate(date = as.POSIXct(date,origin="1970-01-01", tz='UTC')) %>%
  select(date,station,address,postcode,status) %>% 
  distinct()

# combined_df <- combined_df %>%
#   separate(location,sep=',',into=c('latitude','longitude'))

combined_df <- combined_df %>%
  mutate(status = ifelse(status=="Inconnu","Unknown",
                         ifelse(status=="Disponible","Available",
                                ifelse(status=="En maintenance","In maintenance",
                                       ifelse(status=="Occupé (en charge)","Charging",
                                              ifelse(status=="En cours de mise en service","Currently being comissioned",
                                                     ifelse(status=="Supprimé","Deleted",status)))))))

combined_df <- combined_df %>% 
  arrange(date,station)

# combined_df <- combined_df %>%
#   mutate(latitude=as.numeric(latitude),
#          longitude=as.numeric(longitude))



saveRDS(combined_df,'data/V2/occupancy_2022.rds')
write_delim(combined_df,'data/V2/occupancy_2022.csv', delim=',')

### END