### 0. Libraries ###
import os
from my_metric import sae, overall_metric
from sys import argv
import yaml
import shutil
import pandas as pd
import sys
import numpy as np

def mkdir(d):
    if not os.path.exists(d):
        os.makedirs(d)

### Input and Output directories
# root_dir = "/Users/isabelleguyon/Documents/Projects/ParisSaclay/Projects/ChaLab/Examples/iris/"
# default_input_dir = root_dir + "scoring_input_1_2"
# default_output_dir = root_dir + "scoring_output"

# root_dir = "C:/Users/yvenn/OneDrive/Bureau/Thèse/data-exploration/7. Bundle/VE_competition_bundle/4. Starting-kit/"
# default_data_dir = "reference_data_1"
# default_submission_dir = "sample_result_submission_1"
# default_score_dir = "scoring_output"

targets = ["Available","Charging","Passive","Other"]

# =============================== MAIN ========================================

if __name__ == "__main__":

    #### INPUT/OUTPUT: Get input and output directory names
    if len(argv) != 3:  # Use the default input and output directories if no arguments are provided
        # print("Using default directories")
        # print(len(argv))
        # input_dir = default_data_dir
        # output_dir = default_submission_dir
        sys.exit("Input and output directories are not specified correctly")
    else:
        # print("Using given directories")
        input_dir = argv[1]
        public_dates = pd.read_csv(os.path.join(input_dir, "ref", "public_dates.csv"))
        private_dates = pd.read_csv(os.path.join(input_dir, "ref", "private_dates.csv"))
        output_dir = argv[2]
        #score_dir = argv[3]
        # Create the output directory, if it does not already exist and open output files
    mkdir(output_dir)
    score_file_public = open(os.path.join(output_dir, 'score_public.txt'), 'w')
    score_file_private = open(os.path.join(output_dir, 'score_private.txt'), 'w')
    # html_file = open(os.path.join(score_dir, 'scores.html'), 'w')

    ### Loading actual files into dict
    #shutil.unpack_archive(data_dir + '.zip', data_dir)
    actual = {}
    actual['station'] = pd.read_csv(os.path.join(input_dir, "ref", "reference_station.csv"))
    actual['area'] = pd.read_csv(os.path.join(input_dir, "ref", "reference_area.csv"))
    actual['global'] = pd.read_csv(os.path.join(input_dir, "ref", "reference_global.csv"))

    ### Inspecting submission
    # files_list = os.listdir(os.path.join(input_dir, "res"))
    files_list = [ filename for filename in os.listdir(os.path.join(input_dir, "res")) if filename.endswith(".csv")]
    files_number = len(files_list)


    if files_number == 1:
        ### Bottom-up automatic forecast

        ### Loading prediction file
        predicted = {}
        predicted['station'] = pd.read_csv(os.path.join(input_dir, "res", "station.csv"))

        ### Rounding predictions
        predicted['station'][targets] = round(predicted['station'][targets])

        ### Deriving area and global predictions from station forecasts
        predicted['area'] = predicted['station'][['date','area'] + targets].groupby(['date','area'], as_index=False).sum()
        predicted['global'] = predicted['station'][['date'] + targets].groupby(['date'], as_index=False).sum()

    elif files_number == 3:
        ### Normal forecast

        ### Loading prediction files
        predicted = {'station':pd.read_csv(os.path.join(input_dir, "res", "station.csv")),
                     'area':pd.read_csv(os.path.join(input_dir, "res", "area.csv")),
                     'global':pd.read_csv(os.path.join(input_dir, "res", "global.csv")),
        }

        ### Rounding predictions
        predicted['station'][targets] = round(predicted['station'][targets])
        predicted['area'][targets] = round(predicted['area'][targets])
        predicted['global'][targets] = round(predicted['global'][targets])


    else:
        print("The zip archive contains", files_number, "files")
        sys.exit("Error: The zip archive needs to contain either one or three csv files")
    
    if len(actual) != len(predicted):
        sys.exit("Error: The submitted file is not of the same length as the reference file")

    ## Getting final score
    score_public = round(overall_metric(actual, predicted, public_dates),3)
    score_private = round(overall_metric(actual, predicted, private_dates), 3)


    # if np.isnan(score_private):
    #     sys.exit("The score calculated is NaN")

    # Write score corresponding to selected task and metric to the output file
    print(score_public)
    score_file_public.write("score" + ": %0.3f\n" % score_public)
    score_file_public.close()

    print(score_private)
    score_file_private.write("score" + ": %0.3f\n" % score_private)
    score_file_private.close()
