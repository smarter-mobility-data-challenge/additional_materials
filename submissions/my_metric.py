# import numpy as np
import pandas as pd

def sae(y_true, y_pred):
    """Sum of Absolute errors"""
    return(sum(abs(y_true-y_pred)))


def overall_metric(actual_dict, predicted_dict, filter_dates):
    """Overall metric"""
    # predicted_dict['station'] = predicted_dict['station'].sample(frac=1).reset_index(drop=True)
    # actual_dict['station'] = actual_dict['station'].sample(frac=1).reset_index(drop=True)
    import copy
    actual_dict = copy.deepcopy(actual_dict)
    predicted_dict = copy.deepcopy(predicted_dict)

    ### Target list
    targets = ["Available", "Charging", "Passive", "Other"]

    ### Initiating scores list
    scores = []
    ### Filtering dates
    actual_dict['global']['date'] = pd.to_datetime(actual_dict['global']['date'])
    actual_dict['area']['date'] = pd.to_datetime(actual_dict['area']['date'])
    actual_dict['station']['date'] = pd.to_datetime(actual_dict['station']['date'])

    predicted_dict['global']['date'] = pd.to_datetime(predicted_dict['global']['date'])
    predicted_dict['area']['date'] = pd.to_datetime(predicted_dict['area']['date'])
    predicted_dict['station']['date'] = pd.to_datetime(predicted_dict['station']['date'])

    actual_dict['global'] = actual_dict['global'][actual_dict['global']['date'].isin(filter_dates['date'])].sort_values(by = 'date', ascending=True).reset_index(drop=True)
    actual_dict['area'] = actual_dict['area'][actual_dict['area']['date'].isin(filter_dates['date'])].sort_values(by = ['date','area'], ascending=True).reset_index(drop=True)
    actual_dict['station'] = actual_dict['station'][actual_dict['station']['date'].isin(filter_dates['date'])].sort_values(by = ['date','Station'], ascending=True).reset_index(drop=True)

    predicted_dict['global'] = predicted_dict['global'][predicted_dict['global']['date'].isin(filter_dates['date'])].sort_values(by = 'date', ascending=True).reset_index(drop=True)
    predicted_dict['area'] = predicted_dict['area'][predicted_dict['area']['date'].isin(filter_dates['date'])].sort_values(by = ['date','area'], ascending=True).reset_index(drop=True)
    predicted_dict['station'] = predicted_dict['station'][predicted_dict['station']['date'].isin(filter_dates['date'])].sort_values(by = ['date','Station'], ascending=True).reset_index(drop=True)

    ### Number of timesteps in the tet set
    # N = len(actual_dict['global'])
    N = len(filter_dates)
    
    for target in targets:
        # print("==========" + target + "=========")
        # print("global:" + str(sae(actual_dict['global'][target], predicted_dict['global'][target])/N))
        # print("area:" + str(sae(actual_dict['area'][target], predicted_dict['area'][target])/N))
        # print("station:" + str(sae(actual_dict['station'][target], predicted_dict['station'][target])/N))

        scores.append( (sae(actual_dict['global'][target],predicted_dict['global'][target]) +
                        sae(actual_dict['area'][target],predicted_dict['area'][target]) +
                        sae(actual_dict['station'][target],predicted_dict['station'][target]))/N
                      )

    # for level in ["global","area","station"]:
    #     print("==========" + level + "=========")
    #     res = 0
    #     for target in targets:
    #         res = res + sae(actual_dict[level][target], predicted_dict[level][target]) / N
    #     print(res)
    return(sum(scores))
