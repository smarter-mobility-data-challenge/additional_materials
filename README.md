<br />
<p align="center">

  <h1 align="center">Smarter Mobility Data Challenge - Additional Materials</h1>

  <p align="center">
    Exogeneous variables (weather and traffic) and additional records (occupancy) accompanied with sample python and R codes used for data preparation and benchmark evaluation
    <br />
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li><a href="#about-the-project">About The Project</a></li>
	<li><a href="#getting-started">Getting Started</a></li>
    <li><a href="#authors">Authors</a></li>
    <li><a href="#license">License</a></li>
  </ol>
</details>

## About The Project

These additional materials are meant to be used as an extension of the [tutorials](https://gitlab.com/smarter-mobility-data-challenge/tutorials) repository where more details can be found on the smarter mobility challenge.

## Getting Started

This repository contains three folders:

* [data](./data) is an empty repository that should be filled with the data available at [zenodo](https://doi.org/10.5281/zenodo.8280566).
* [scripts](./scripts) contains the scripts used to prepare the data as well as the benchmark submissions.
* [submissions](./submissions) contains the submissions (predictions) from the catboost model using temporal features augmented with exogeneous information.

## Authors
* **Nathan Doumèche** - (https://nathandoumeche.com/)
* **Yannig Goude** - (https://www.imo.universite-paris-saclay.fr/~goude/about.html)
* **Yvenn Amara-Ouali** - (https://www.yvenn-amara.com)

## License
* The data that can be found at [zenodo](https://doi.org/10.5281/zenodo.8280566) is licensed under the Creative Commons Attribution 4.0 International
* The codes associated are licensed under the GNU GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details
